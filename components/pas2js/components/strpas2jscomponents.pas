unit strpas2jscomponents;

{$mode objfpc}{$H+}

interface

Resourcestring
  rsActionListComponentEditor = 'HTM&L Element Actionlist Editor...';
  rsActionListCreateMissing = 'Create &actions for HTML tags...';
  rsErrNoHTMLFileNameForComponent = 'No HTML filename found for component %s';
  rsHTMLActionsCreated = '%d HTML Element Action components were created';

implementation

end.

